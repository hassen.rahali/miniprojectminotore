package com.example.bookMicroservice.beans;


import lombok.Data;

@Data
public class SaleBean {
    
    private Long unitsSold ;
    private String libraryName ; 
    private String bookName ;
}
