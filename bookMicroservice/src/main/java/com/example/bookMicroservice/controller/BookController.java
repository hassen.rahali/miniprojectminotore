package com.example.bookMicroservice.controller;


import com.example.bookMicroservice.dto.BookDto;
import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.services.BookService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.websocket.server.PathParam;
import java.util.List;

/**
 * A controller that handles all http request related to book
 */


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class BookController {
    /**
     * service that englobe all methods related to class BOOK
     */
    @Autowired
    BookService bookService;


    /**
     * return book properties by book name
     * @param name book name
     * @return
     */
    @Operation(summary = "Get books",
            description = "Get list of users")
    @GetMapping(value = "/getbookname/{name}")
    public Book getbookByName(@PathParam(value = "name")String name ){

        return bookService.getBookByName(name);
    }


    /**
     * This method returns book by id
     * @param id id of the requested book
     * @return book with id= to id in param
     */
    @GetMapping(value = "/book/{id}")
    public Book getBokkById (@PathParam(value = "id")Long id ){
        return bookService.getBookById(id);
    }

    /**
     * create a new book
     * @param book book you want to create
     * @return the created book in case of creation
     */
    @SneakyThrows
    @PostMapping(value = "/book")
    public Book addBook ( @RequestBody Book book){
        return bookService.addBook(book);
    }


    /**
     * delete book from database
     * @param book book you want to delete
     */
    @DeleteMapping(value = "book")
    public void delete(Book book){
        bookService.deleteBook(book);
    }




}
