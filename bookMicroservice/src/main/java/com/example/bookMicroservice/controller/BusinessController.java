package com.example.bookMicroservice.controller;


import com.example.bookMicroservice.beans.SaleBean;
import com.example.bookMicroservice.dto.BookDto;
import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.mapper.BookDtoMapper;
import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.models.Magazine;
import com.example.bookMicroservice.services.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BusinessController {
    @Autowired
    BusinessService businessService;

    @GetMapping(value = "books/{name}")
    public List<BookDto> getAllBooksByLibraryName(@PathVariable(value = "name") String name){

       return businessService.getBooksByLibraryName(name).stream()
               .map(book -> BookDtoMapper.bookToBookDTO(book))
               .collect(Collectors.toList());
    }

    @PostMapping(value = "/saleValidation")
    public Boolean saleValidation(@RequestBody SaleBean saleBean)
    {
        return businessService.saleValidation(saleBean);
    }


    @PostMapping (value = "/doSale")
    public SaleBean doSale(@RequestBody SaleBean saleBean)
    {
        return businessService.doSale(saleBean);
    }

    @GetMapping(value = "getBooksByLibrary/{name}")
    List<Book> getBooksByLibrary (@PathVariable(value = "name") String name) {

        return  businessService.getBooksByLibraryName(name);
    }

    @GetMapping(value = "getNovelsByLibrary/{name}")
    List<NovelDto> getNovelsByLibrary (@PathVariable(value = "name") String name) {

        return  businessService.getNovelsByLibraryName(name);
    }

    @GetMapping(value = "getMagazinesByLibrary/{name}")
    List<MagazineDto> getMagazinesByLibrary (@PathVariable(value = "name") String name) {

        return  businessService.getMagazinesByLibraryName(name);
    }


    @GetMapping(value = "mostSoldByAuthor/{id}")
    Book getBooksByLibrary (@PathVariable(value = "id") Long id) {

        return  businessService.mostSoldNovelByAuthor(id);
    }

    @GetMapping(value = "recentMagazine")
     Magazine getRecentMagazine () {

        return businessService.recentCookiesMagazine();
    }
}
