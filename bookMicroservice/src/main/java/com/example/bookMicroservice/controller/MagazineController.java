package com.example.bookMicroservice.controller;


import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.services.MagazineService;
import com.example.bookMicroservice.services.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class MagazineController {

    @Autowired
    MagazineService magazineService;

    @GetMapping(value="/magazines")
    public List<MagazineDto> getAll (){
        return magazineService.getAll();
    }

}
