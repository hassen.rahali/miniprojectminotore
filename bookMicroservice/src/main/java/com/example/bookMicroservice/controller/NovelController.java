package com.example.bookMicroservice.controller;


import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.models.Novel;
import com.example.bookMicroservice.services.NovelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class NovelController {



    @Autowired
    NovelService novelService;

    @GetMapping(value="/novels")
    public List<NovelDto> getAll (){
        return novelService.getAll();
    }

}
