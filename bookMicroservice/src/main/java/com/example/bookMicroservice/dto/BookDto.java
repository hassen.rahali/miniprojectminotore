package com.example.bookMicroservice.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class BookDto {
    private String name ;
    private Long totalUnitSold ;
    private Double price ;
}
