package com.example.bookMicroservice.dto;

import com.example.bookMicroservice.models.BookCategory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Set;

@Slf4j
@Data
public class MagazineDto {

    private String name ;
    private String author;
    private BookCategory bookCategory ;
    private Integer numberOfPages ;
    private Double price ;
    public String netReleaseDate ;
}
