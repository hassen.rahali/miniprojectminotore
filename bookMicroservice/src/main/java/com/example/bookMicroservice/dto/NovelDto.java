package com.example.bookMicroservice.dto;

import com.example.bookMicroservice.models.Author;
import com.example.bookMicroservice.models.BookCategory;
import lombok.Data;


@Data
public class NovelDto {
    private String name ;
    private String author;
    private BookCategory bookCategory ;
    private Integer numberOfPages ;
    private Double price ;
    private String storySummary ;






}
