package com.example.bookMicroservice.exceptions;

import javax.management.InstanceAlreadyExistsException;

public class ObjectAlreadyExistsException extends InstanceAlreadyExistsException {
}
