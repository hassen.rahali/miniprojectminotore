package com.example.bookMicroservice.mapper;

import com.example.bookMicroservice.dto.BookDto;
import com.example.bookMicroservice.models.Book;
import org.modelmapper.ModelMapper;

public class BookDtoMapper {



    public static final BookDto bookToBookDTO(Book book) {
        ModelMapper modelMapper = new ModelMapper();
        BookDto bookDto =modelMapper.map(book, BookDto.class);
        return bookDto;
    }
    public static final Book bookDtoToBook(BookDto bookDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(bookDto, Book.class);
    }
}
