package com.example.bookMicroservice.mapper;

import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.models.Magazine;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import java.text.SimpleDateFormat;

@Slf4j
public class MagazineDtoMapper {



    public static final MagazineDto magazineToMagazineDTO(Magazine magazine) {
    ModelMapper modelMapper = new ModelMapper();

    MagazineDto magazineDto =modelMapper.map(magazine, MagazineDto.class);
    magazineDto.setAuthor(magazine.getAuthor().getName());
    magazineDto.setNetReleaseDate(magazineDto.getNetReleaseDate().substring(0,10));

    return magazineDto;
}
    public static final Magazine magazineDtoToMagazine(MagazineDto magazineDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(magazineDto, Magazine.class);
    }
}
