package com.example.bookMicroservice.mapper;

import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.models.Novel;
import org.modelmapper.ModelMapper;

public class NovelDtoMapper {
    public static final NovelDto novelToNovelDTO(Novel novel) {
        ModelMapper modelMapper = new ModelMapper();
        NovelDto novelDto =modelMapper.map(novel, NovelDto.class);
        novelDto.setAuthor(novel.getAuthor().getName());
        return novelDto;
    }
    public static final Novel novelDtoToNovel(NovelDto novelDto) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(novelDto, Novel.class);
    }
}
