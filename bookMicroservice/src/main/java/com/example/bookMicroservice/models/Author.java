package com.example.bookMicroservice.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id  ;


    @Column(name = "name")
    String name ;


    @OneToMany(mappedBy="author")
    private Set<Book> books;
}
