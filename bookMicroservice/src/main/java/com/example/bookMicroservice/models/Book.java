package com.example.bookMicroservice.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;








@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="book_type",
        discriminatorType = DiscriminatorType.STRING)
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id ;


    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="author_id", nullable=false)
    private Author author;


    @Column(name = "Name")
    String name ;

    @Column(name = "Price")
    Double price ;

    @Column(name = "TotalUnitSold")
    Long totalUnitSold ;

    @Column(name = "PublicationDate")
    Date publicationDate ;

    @Column(name = "NumberOfPages")
    Integer numberOfPages ;


    //Book category enumeration of strings
    @Enumerated(EnumType.STRING)
    private BookCategory bookCategory ;




}





