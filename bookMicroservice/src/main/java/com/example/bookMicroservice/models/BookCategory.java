package com.example.bookMicroservice.models;

public enum BookCategory {
    Historical,
    Crime ,
    Fashion ,
    Fiction ,
    Cooking
}