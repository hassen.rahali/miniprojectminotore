package com.example.bookMicroservice.models;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
public class Library  {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Long id ;

    @Column(name = "name")
    private String name ;

    @Column(name = "adress")
    private String adress ;


}
