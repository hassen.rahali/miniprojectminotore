package com.example.bookMicroservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.Date;
import java.util.Set;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("MAGAZINE")
public class Magazine extends Book {

    private Date netReleaseDate ;

    @ElementCollection
    private Set<String> keySubjects ;

}
