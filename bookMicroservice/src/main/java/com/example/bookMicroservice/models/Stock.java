package com.example.bookMicroservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class Stock {

    @EmbeddedId
    StockKey stockKey;
    @JsonIgnore
    @ManyToOne
    @MapsId("bookid")
    @JoinColumn(name = "book_id")
    Book book;


    @JsonIgnore
    @ManyToOne
    @MapsId("libraryid")
    @JoinColumn(name = "library_id")
    Library library;

    Long quantity;




}
