package com.example.bookMicroservice.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


@Embeddable
@Data
public class StockKey implements Serializable {

    @Column(name = "book_id")
    Long bookId;

    @Column(name = "library_id")
    Long libraryId;
}
