package com.example.bookMicroservice.repositories;

import com.example.bookMicroservice.models.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    Author findByName(String name);

    boolean existsByName(String name);
}
