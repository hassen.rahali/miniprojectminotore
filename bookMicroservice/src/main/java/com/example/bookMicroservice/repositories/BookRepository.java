package com.example.bookMicroservice.repositories;

import com.example.bookMicroservice.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepository extends JpaRepository<Book,Long> {
    Book findByName(String name);

    boolean existsByName(String name);



}
