package com.example.bookMicroservice.repositories;

import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.models.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface LibraryRepository extends JpaRepository<Library,Long> {
    Optional<Library> findByName(String libraryName);

    boolean existsByName(String name);
    List<Book> findBookByName(String name);
}
