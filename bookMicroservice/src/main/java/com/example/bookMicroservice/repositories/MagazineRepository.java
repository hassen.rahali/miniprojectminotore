package com.example.bookMicroservice.repositories;

import com.example.bookMicroservice.models.BookCategory;
import com.example.bookMicroservice.models.Magazine;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface MagazineRepository extends JpaRepository<Magazine,Long> {

    List<Magazine> findByBookCategory(BookCategory bookCategory);
}
