package com.example.bookMicroservice.repositories;

import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.models.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface StockRepository extends CrudRepository<Stock,Long> {
    List<Stock> findByStockKeyLibraryId(Long id);
    Boolean existsByLibraryNameAndBookNameAndQuantityIsGreaterThanEqual(String library ,String book, Long quantity);
    Stock findByLibraryNameAndBookName(String library , String book);

    List<Book> findByLibraryName(String name);
}
