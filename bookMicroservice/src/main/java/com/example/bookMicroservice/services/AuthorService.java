package com.example.bookMicroservice.services;


import com.example.bookMicroservice.exceptions.ObjectAlreadyExistsException;
import com.example.bookMicroservice.exceptions.ObjectNotFoundException;
import com.example.bookMicroservice.models.Author;
import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.repositories.AuthorRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Slf4j
@Service
public class AuthorService {

    @Autowired
    AuthorRepository authorRepository;


    //return author By name
    public Author getAuthorByName(String name){

        log.info("Author with name"+name +"fetched");
        return authorRepository.findByName( name);

    }

    //return Author By id
    public Author getAuthorById(Long id){

        log.info("Author with id"+id +"fetched");
        Optional<Author> author = authorRepository.findById( id);
        if(author.isPresent()){
            log.info("Author with id"+id +"was found");
            return author.get();
        }else {
            log.error("author with id = "+ id + "not found");
            throw new ObjectNotFoundException();
        }
    }


    //get all authors
    public List<Author> getAllAuthors(){
        log.info("list of all authors fetched");
        return authorRepository.findAll();

    }

    public Author addAuthor(Author author) throws ObjectAlreadyExistsException {

        if(authorRepository.existsByName(author.getName())){
            log.error("Author already exists");
            throw new ObjectAlreadyExistsException();
        }else {
            log.info("a new author has been saved to the database");
            return author ;
        }
    }

    public void deleteAuthor(Author author){
        log.info("BOOK DELETED");
        authorRepository.delete(author);
    }

}