package com.example.bookMicroservice.services;


import com.example.bookMicroservice.dto.BookDto;
import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.exceptions.ObjectAlreadyExistsException;
import com.example.bookMicroservice.exceptions.ObjectNotFoundException;
import com.example.bookMicroservice.mapper.BookDtoMapper;
import com.example.bookMicroservice.mapper.MagazineDtoMapper;
import com.example.bookMicroservice.models.Book;
import com.example.bookMicroservice.models.Library;
import com.example.bookMicroservice.models.Magazine;
import com.example.bookMicroservice.models.Stock;
import com.example.bookMicroservice.repositories.BookRepository;
import com.example.bookMicroservice.repositories.LibraryRepository;
import com.example.bookMicroservice.repositories.StockRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    StockRepository stockRepository;

    @Autowired
    LibraryRepository libraryRepository;


    /**
     * get book by book name
     * @param name book name
     * @return
     */
    public Book getBookByName(String name){

        log.info("Book with name"+name +"fetched");
        return bookRepository.findByName( name);

    }

    //return Book By id
    public Book getBookById(Long id){

        log.info("Book with id"+id +"fetched");
        Optional<Book> book = bookRepository.findById( id);
        if(book.isPresent()){
            log.info("Book with id"+id +"was found");
            return book.get();
        }else {
            log.error("book with id = "+ id + "not found");
            throw new ObjectNotFoundException();
        }
    }



    public Book addBook(Book book) throws ObjectAlreadyExistsException {

       if(bookRepository.existsByName(book.getName())){
           log.error("Book already exists");
           throw new ObjectAlreadyExistsException();
       }else {
           log.info("a new book has been saved to the database");
           return book ;
       }
    }

    public void deleteBook(Book book){
        log.info("BOOK DELETED");
        bookRepository.delete(book);
    }

}
