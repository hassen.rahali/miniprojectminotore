package com.example.bookMicroservice.services;


import com.example.bookMicroservice.beans.SaleBean;
import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.exceptions.ObjectNotFoundException;
import com.example.bookMicroservice.mapper.MagazineDtoMapper;
import com.example.bookMicroservice.mapper.NovelDtoMapper;
import com.example.bookMicroservice.models.*;
import com.example.bookMicroservice.repositories.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
@Slf4j
@Service
public class BusinessService {

    @Autowired
    private final BookRepository bookRepository;

    @Autowired
    private final LibraryRepository libraryRepository;

    @Autowired
    private final StockRepository stockRepository;

    @Autowired
    private final AuthorRepository authorRepository;

    @Autowired
    private final MagazineRepository magazineRepository;

    @Autowired
    public BusinessService(BookRepository bookRepository, LibraryRepository libraryRepository,
                           StockRepository stockRepository, AuthorRepository authorRepository, MagazineRepository magazineRepository) {
        this.bookRepository = bookRepository;
        this.libraryRepository = libraryRepository;
        this.stockRepository = stockRepository;
        this.authorRepository = authorRepository;
        this.magazineRepository = magazineRepository;
    }

    public List<Book> getAllBooks() {

        return bookRepository.findAll();
    }


    public Book getBookById(Long id) throws ChangeSetPersister.NotFoundException {
        Optional<Book> book = bookRepository.findById(id);

        if (book.isPresent()) {
            return book.get();
        } else {
            throw new ObjectNotFoundException();
        }

    }

    public Boolean saleValidation(SaleBean saleBean) throws ObjectNotFoundException {


       return stockRepository.existsByLibraryNameAndBookNameAndQuantityIsGreaterThanEqual(
               saleBean.getLibraryName(),
               saleBean.getBookName(),
               saleBean.getUnitsSold());
    }


    public List<Book> getBooksByLibraryName(String libraryName) {
        Optional<Library> library = libraryRepository.findByName(libraryName);

        if (library.isPresent()) {
            List<Stock> libraryStocks = stockRepository.findByStockKeyLibraryId(library.get().getId());
            List<Book> bookList = libraryStocks.stream()
                    .map(stock -> {
                        return stock.getBook();
                    })
                    .collect(Collectors.toList());
            return bookList;

        } else
            throw new ObjectNotFoundException();
    }


    public List<NovelDto> getNovelsByLibraryName(String libraryName) {
        Optional<Library> library = libraryRepository.findByName(libraryName);

        if (library.isPresent()) {
            List<Stock> libraryStocks = stockRepository.findByStockKeyLibraryId(library.get().getId());
            List<NovelDto> bookList = libraryStocks.stream()
                    .map(stock -> {
                        return stock.getBook();
                    })
                    .filter(book -> book instanceof Novel)
                    .map(novel -> NovelDtoMapper.novelToNovelDTO((Novel) novel))
                    .collect(Collectors.toList());
            return bookList;

        } else
            throw new ObjectNotFoundException();
    }



    public List<MagazineDto> getMagazinesByLibraryName(String libraryName) {
        Optional<Library> library = libraryRepository.findByName(libraryName);

        if (library.isPresent()) {
            List<Stock> libraryStocks = stockRepository.findByStockKeyLibraryId(library.get().getId());
            List<MagazineDto> bookList = libraryStocks.stream()
                    .map(stock -> {
                        return stock.getBook();
                    })
                    .filter(book -> book instanceof Magazine)
                    .map(novel -> MagazineDtoMapper.magazineToMagazineDTO((Magazine) novel))
                    .collect(Collectors.toList());
            return bookList;

        } else
            throw new ObjectNotFoundException();
    }

    public Book mostSoldNovelByAuthor(Long id) {
        Optional<Author> author = authorRepository.findById(id);

        if (author.isPresent()) {
            Set<Book> books = author.get().getBooks();

            Book novel = books.stream()
                    .filter(book -> book instanceof Novel)
                    .max(Comparator.comparing(Book::getTotalUnitSold))
                    .orElseThrow(ObjectNotFoundException::new);

            return novel;


        } else {

            throw new ObjectNotFoundException();
        }

    }

    public Magazine recentCookiesMagazine() {
        List<Magazine> magazines = magazineRepository.findByBookCategory( BookCategory.Crime);
        Optional<Magazine> magazine=magazines.stream()
        .sorted(Comparator.comparing(Magazine::getNetReleaseDate))
                .findFirst();

        if (magazine.isPresent()){
            log.trace("A TRACE Message");
            log.debug("A DEBUG Message");
            log.info("An INFO Message");
            log.warn("A WARN Message");
            log.error("An ERROR Message");
            return  magazine.get();
        } else
            throw new  ObjectNotFoundException();




    }

    public SaleBean doSale (SaleBean saleBean){
        Stock stock = stockRepository.findByLibraryNameAndBookName(
                saleBean.getLibraryName(),
                saleBean.getBookName()
        );

        stock.setQuantity(
                stock.getQuantity()-saleBean.getUnitsSold()
        );

        stockRepository.save(stock);
        return saleBean;

    }

}




