package com.example.libraryMicroservice.services;

import com.example.bookMicroservice.exceptions.ObjectAlreadyExistsException;
import com.example.bookMicroservice.exceptions.ObjectNotFoundException;
import com.example.bookMicroservice.models.Library;
import com.example.bookMicroservice.repositories.LibraryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class LibraryService {
    
    @Autowired
    LibraryRepository libraryRepository;


    //return Library By name
    public Library getLibraryByName(String name){

        log.info("Library with name"+name +"fetched");
            Optional<Library> library =libraryRepository.findByName( name);
            if (library.isPresent()){
                return library.get();
            }else {
                log.warn("libraray having name "+name +"not found");
                throw new ObjectNotFoundException();
            }

    }

    //return Library By id
    public Library getLibraryById(Long id){

        log.info("Library with id"+id +"fetched");
        Optional<Library> library = libraryRepository.findById( id);
        if(library.isPresent()){
            log.info("Library with id"+id +"was found");
            return library.get();
        }else {
            log.error("library with id = "+ id + "not found");
            throw new ObjectNotFoundException();
        }
    }


    /**
     * get all libraries
     * @return list of libraries
     */
    public List<Library> getAllLibrarys(){
        log.info("list of all librarys fetched");
        return libraryRepository.findAll();

    }

    /**
     * add library to the database
     * @param library
     * @return library if added
     * @throws ObjectAlreadyExistsException
     */
    public Library addLibrary(Library library) throws ObjectAlreadyExistsException {

        if(libraryRepository.existsByName(library.getName())){
            log.error("Library already exists");
            throw new ObjectAlreadyExistsException();
        }else {
            log.info("a new library has been saved to the database");
            return library ;
        }
    }

    public void deleteLibrary(Library library){
        log.info("BOOK DELETED");
        libraryRepository.delete(library);
    }

}
