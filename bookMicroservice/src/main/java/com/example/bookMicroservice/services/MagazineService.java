package com.example.bookMicroservice.services;

import com.example.bookMicroservice.dto.MagazineDto;
import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.mapper.MagazineDtoMapper;
import com.example.bookMicroservice.mapper.NovelDtoMapper;
import com.example.bookMicroservice.models.Magazine;
import com.example.bookMicroservice.models.Novel;
import com.example.bookMicroservice.repositories.MagazineRepository;
import com.example.bookMicroservice.repositories.NovelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class MagazineService {


    @Autowired
    MagazineRepository magazineRepository;

    public List<MagazineDto> getAll (){
        List<Magazine>  novels =  magazineRepository.findAll();
        List <MagazineDto> magazineDto = novels.stream()
                .map(magazine -> MagazineDtoMapper.magazineToMagazineDTO(magazine))
                .collect(Collectors.toList());
        ;
        return magazineDto;
    }
}
