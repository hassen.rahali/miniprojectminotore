package com.example.bookMicroservice.services;


import com.example.bookMicroservice.dto.NovelDto;
import com.example.bookMicroservice.mapper.NovelDtoMapper;
import com.example.bookMicroservice.models.Novel;
import com.example.bookMicroservice.repositories.NovelRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class NovelService {


    @Autowired
    NovelRepository novelRepository;

    public List<NovelDto> getAll (){
        List<Novel>  novels =  novelRepository.findAll();
        List <NovelDto> novelDtos = novels.stream()
                .map(novel -> NovelDtoMapper.novelToNovelDTO(novel))
                .collect(Collectors.toList());
               ;
        return novelDtos;
    }
}
