package com.minotore.saleMicroservice;

import com.minotore.saleMicroservice.models.Sale;
import com.minotore.saleMicroservice.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
@EnableEurekaClient
@EnableFeignClients
@EnableMongoRepositories
@SpringBootApplication
public class SaleMicroserviceApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SaleMicroserviceApplication.class, args);
	}

	@Autowired
	private SaleRepository saleRepository;

	@Override
	public void run(String... args) throws Exception {
		Sale sale = new Sale(new Long(1),"hh","jjj",new Double (5),new Long(8));
		saleRepository.save(sale);
		Sale sale1 = new Sale(new Long(2),"hh","jjj",new Double (5),new Long(8));
		saleRepository.save(sale1);
	}
}
