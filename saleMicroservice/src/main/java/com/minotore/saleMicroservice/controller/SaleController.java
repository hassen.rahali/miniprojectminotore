package com.minotore.saleMicroservice.controller;


import com.minotore.saleMicroservice.models.Sale;
import com.minotore.saleMicroservice.services.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaleController {
    @Autowired
    SaleService saleService ;

    @PostMapping(value = "sale")
    public ResponseEntity<Sale> doSale (@RequestBody Sale sale){
        return saleService.doSale(sale);

    }
}
