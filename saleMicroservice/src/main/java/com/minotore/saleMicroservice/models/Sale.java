package com.minotore.saleMicroservice.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;



@Data
@AllArgsConstructor
public class Sale  {

    @Id
    private Long id ;
    private String libraryName ;
    private String bookName ;
    private Double bookPrice ;
    private Long unitsSold;
}
