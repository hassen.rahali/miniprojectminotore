package com.minotore.saleMicroservice.proxy;


import com.minotore.saleMicroservice.models.Sale;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("book-service")
public interface BookAppProxy {


    @PostMapping(value = "/saleValidation")
    public Boolean saleValidation(@RequestBody Sale sale);

    @PostMapping (value = "/doSale")
    public Sale doSale(@RequestBody Sale sale);




}
