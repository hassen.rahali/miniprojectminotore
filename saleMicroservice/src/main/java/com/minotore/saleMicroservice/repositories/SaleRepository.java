package com.minotore.saleMicroservice.repositories;

import com.minotore.saleMicroservice.models.Sale;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SaleRepository extends MongoRepository<Sale, Long> {
}
