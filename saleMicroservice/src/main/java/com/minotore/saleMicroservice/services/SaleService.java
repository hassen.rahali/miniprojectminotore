package com.minotore.saleMicroservice.services;


import com.minotore.saleMicroservice.models.Sale;
import com.minotore.saleMicroservice.proxy.BookAppProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class SaleService {

    @Autowired
    BookAppProxy bookAppProxy;

    /**
     * *
     * @param sale
     * @return
     */
    public ResponseEntity<Sale> doSale(Sale sale){
        if (bookAppProxy.saleValidation(sale)){
                bookAppProxy.doSale(sale);
        }else {
            log.error("can't do sale due to quatity");
            throw new IllegalArgumentException("stock not sufficiant");
        }
        return new ResponseEntity<Sale>(sale, HttpStatus.OK);

    }

}
